﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SchedulerExample;

namespace SchedulerExample.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(DateTime.Now);
            Console.ResetColor();

            var cancellationTokenSource = new CancellationTokenSource();

            IJobService job1 = JobManager.Scheduler()
                .ToRunOnceIn(1)
                .Seconds()
                .AtStartTime()
                .BuildJobService<MyJob>();


            IJobService job2 = JobManager.Scheduler()
                .ToRunOnceIn(2)
                .Seconds()
                .AtStartTime()
                .BuildJobService<MyJob1>();


            job1.Start(cancellationTokenSource.Token);
            job2.Start(cancellationTokenSource.Token);


            Console.ReadLine();
            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();


            Console.ReadLine();
        }
    }

    public class MyJob : IJob
    {
        string _path;
        TimeSpan _dueTime;
        TimeSpan _period;

        public MyJob(string path)
        {
            _path = path;
        }

        public MyJob(TimeSpan dueTime, TimeSpan period)
        {
            _dueTime = dueTime;
            _period = period;
        }
        public MyJob() { }

        public void Execute()
        {
            Console.WriteLine(DateTime.Now.ToString("yyyy MM dd HH:mm:ss"));
        }
    }
    public class MyJob1 : IJob
    {
        string _path;
        TimeSpan _dueTime;
        TimeSpan _period;

        public MyJob1(string path)
        {
            _path = path;
        }

        public MyJob1(TimeSpan dueTime, TimeSpan period)
        {
            _dueTime = dueTime;
            _period = period;
        }
        public MyJob1() { }

        public void Execute()
        {
            Console.WriteLine("Barevner");
        }
    }
}
