﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerExample
{
    public class JobService<TJob> : IJobService
        where TJob : IJob, new()
    {
        private readonly IJob _job;
        private Timer _timer;
        int _dueTime;
        int _period;

        public JobService(int dueTime, int period)
        {
            _dueTime = dueTime;
            _period = period;
            _job = (IJob)Activator.CreateInstance(typeof(TJob));
        }

        public JobService(int dueTime, int period,Func<TJob> createJob)
        {
            _dueTime = dueTime;
            _period = period;
            _job = createJob();
        }

        public void Start(CancellationToken cancellationToken)
        {
            _timer = new Timer(p =>
            {
                if (cancellationToken.IsCancellationRequested)
                    Dispose();
                else
                    _job.Execute();
            }, null, _dueTime, _period);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            _timer?.Change(Timeout.Infinite, 0);
        }

        public Task StopAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> ChangeAsync(int dueTime, int period)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                Stop();
                _timer = null;
            }
        }
    }
}