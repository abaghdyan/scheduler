﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerExample
{
    public interface IJobService : IDisposable
    {
        void Stop();
        Task StopAsync();
        void Start(CancellationToken cancellationToken);
        Task StartAsync(CancellationToken cancellationToken);
        Task<bool> ChangeAsync(int dueTime, int period);
    }
}