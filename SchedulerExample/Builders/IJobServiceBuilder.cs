﻿using System;

namespace SchedulerExample.Builders
{
    public interface IJobServiceBuilder
    {
        IJobService BuildJobService<TJob>() where TJob : IJob, new();
        IJobService BuildJobService<TJob>(Func<TJob> createJob) where TJob : IJob, new();
    }
}