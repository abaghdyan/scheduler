﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulerExample
{
    enum TimeUnit
    {
        Second = 1000,
        Minute = Second * 60,
        Hour = Minute * 60,
        Day = Hour * 24
    }
}
